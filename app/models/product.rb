class Product < ActiveRecord::Base

  #validate that all fields are not empty
  validates :title, :description, :image_url, presence: true
  #we validate tha price is a valid number
  validates :price, numericality: {greater_than_or_equal_to: 0.01}

  #validates if a paticular row is unique
  validates :title, uniqueness: true

  validates :image_url, allow_blank: true, format: {
with:
%r{\.(gif|jpg|png)\Z}i,
message: 'must be a URL for GIF, JPG or PNG image.'
}

end
